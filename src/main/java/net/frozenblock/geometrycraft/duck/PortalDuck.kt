package net.frozenblock.geometrycraft.duck

import net.minecraft.world.phys.Vec3

interface PortalDuck {

    fun `geometryDash$getGDPortalPos`(): Vec3?

    fun `geometryDash$setGDPortalPos`()
}
