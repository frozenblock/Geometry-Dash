package net.frozenblock.geometrycraft.duck

import net.frozenblock.geometrycraft.data.GDData

@Suppress("FunctionName")
interface PlayerDuck {

    fun `geometryDash$getGDData`(): GDData
    fun `geometryDash$updateSyncedGDData`()
    fun `geometryDash$syncOnlyGDData`()
}
