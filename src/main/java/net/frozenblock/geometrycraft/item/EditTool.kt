package net.frozenblock.geometrycraft.item

import net.frozenblock.geometrycraft.entity.Checkpoint
import net.frozenblock.geometrycraft.entity.Orb
import net.frozenblock.geometrycraft.entity.Portal
import net.frozenblock.geometrycraft.item.EditTool.Mode.Companion.getMode
import net.frozenblock.geometrycraft.item.EditTool.Mode.Companion.putMode
import net.frozenblock.geometrycraft.item.EditTool.Type.Companion.getType
import net.frozenblock.geometrycraft.item.EditTool.Type.Companion.putType
import net.frozenblock.geometrycraft.registry.RegisterEntities
import net.minecraft.nbt.CompoundTag
import net.minecraft.util.StringRepresentable
import net.minecraft.world.InteractionHand
import net.minecraft.world.InteractionResult
import net.minecraft.world.InteractionResultHolder
import net.minecraft.world.entity.player.Player
import net.minecraft.world.item.Item
import net.minecraft.world.item.ItemStack
import net.minecraft.world.level.Level

/**
 * A tool for creating, editing, and deleting GD entities
 */
open class EditTool(props: Properties = Properties().stacksTo(1)) : Item(props) {

    override fun use(level: Level, player: Player, usedHand: InteractionHand): InteractionResultHolder<ItemStack> {
        val stack = player.getItemInHand(usedHand)
        if (!player.isCreative) return InteractionResultHolder.fail(stack)

        /*val tag = stack.getOrCreateTag()
        val type = tag.getType("type")
        val mode = tag.getMode("mode")

        // switch modes when shifting
        if (player.isShiftKeyDown) {
            if (!level.isClientSide) {
                val newTag = CompoundTag()
                // TODO: switch type from crouch + left click
                newTag.putType("type", type.next())
                newTag.putMode("mode", mode.next())
                stack.setTag(newTag)
                return InteractionResultHolder(InteractionResult.SUCCESS, stack)
            }
        }

        when (mode) {
            Mode.CREATE -> {
                when (type) {
                    Type.SPIKE -> {
                        // TODO: implement
                    }
                    Type.ORB -> {
                        val orb = Orb(RegisterEntities.ORB, level)
                        orb.type = Orb.OrbType.BOUNCE
                        orb.setPos(player.position())
                        if (level.addFreshEntity(orb)) {
                            return InteractionResultHolder.success(stack)
                        }
                    }
                    Type.PORTAL -> {
                        val portal = Portal(RegisterEntities.PORTAL, level)
                        portal.type = Portal.PortalType.SHIP
                        portal.setPos(player.position())
                        if (level.addFreshEntity(portal)) {
                            return InteractionResultHolder.success(stack)
                        }
                    }
                    Type.CHECKPOINT -> {
                        val checkpoint = Checkpoint(RegisterEntities.CHECKPOINT, level)
                        checkpoint.type = Checkpoint.CheckpointType.STANDARD
                        checkpoint.setPos(player.position())
                        if (level.addFreshEntity(checkpoint)) {
                            return InteractionResultHolder.success(stack)
                        }
                    }
                }
            }
            else -> {} // TODO: implement
        }*/

        return super.use(level, player, usedHand)
    }

    // the type of block/entity to operate with
    enum class Type : StringRepresentable {
        SPIKE,
        ORB,
        PORTAL,
        CHECKPOINT;

        companion object {
            @JvmField
            val CODEC: StringRepresentable.EnumCodec<Type> = StringRepresentable.fromEnum(::values)

            fun CompoundTag.getType(key: String): Type {
                return CODEC.byName(this.getString(key)) ?: SPIKE
            }

            fun CompoundTag.putType(key: String, type: Type): CompoundTag {
                this.putString(key, type.serializedName)
                return this
            }
        }

        fun next(): Type = when (this) {
            SPIKE -> ORB
            ORB -> PORTAL
            PORTAL -> CHECKPOINT
            CHECKPOINT -> SPIKE
        }

        override fun getSerializedName(): String = this.name.lowercase()
    }

    enum class Mode : StringRepresentable {
        CREATE, // create a new object
        EDIT, // edit object properties (ex: portal type)
        MOVE,
        COPY;

        companion object {
            @JvmField
            val CODEC: StringRepresentable.EnumCodec<Mode> = StringRepresentable.fromEnum(::values)

            fun CompoundTag.getMode(key: String): Mode {
                return CODEC.byName(this.getString(key)) ?: CREATE
            }

            fun CompoundTag.putMode(key: String, mode: Mode): CompoundTag {
                this.putString(key, mode.serializedName)
                return this
            }
        }

        fun next(): Mode = when (this) {
            CREATE -> EDIT
            EDIT -> MOVE
            MOVE -> COPY
            COPY -> CREATE
        }

        override fun getSerializedName(): String = this.name.lowercase()
    }
}
