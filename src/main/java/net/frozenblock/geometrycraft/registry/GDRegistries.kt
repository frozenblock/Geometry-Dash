package net.frozenblock.geometrycraft.registry

import com.mojang.serialization.Codec
import com.mojang.serialization.MapCodec
import net.frozenblock.geometrycraft.data.mode.*
import net.frozenblock.geometrycraft.util.id
import net.fabricmc.fabric.api.event.registry.FabricRegistryBuilder
import net.fabricmc.fabric.api.event.registry.RegistryAttribute
import net.minecraft.core.Registry
import net.minecraft.resources.ResourceKey

object GDRegistries {

    @JvmField
    val GD_MODE_DATA: Registry<MapCodec<out GDModeData>> = FabricRegistryBuilder.createDefaulted<MapCodec<out GDModeData>>(
        ResourceKey.createRegistryKey(id("gd_mode_data")),
        id("cube")
    ).attribute(RegistryAttribute.SYNCED).buildAndRegister()

    init {
        GDModeData.bootstrap(GD_MODE_DATA)
    }
}
