package net.frozenblock.geometrycraft.data.mode

import com.mojang.serialization.Codec
import com.mojang.serialization.MapCodec
import net.frozenblock.geometrycraft.data.GDMode
import net.frozenblock.geometrycraft.entity.pose.GDPoses
import net.frozenblock.geometrycraft.util.gdGravity
import net.frozenblock.geometrycraft.util.launch
import net.minecraft.nbt.CompoundTag
import net.minecraft.util.Mth
import net.minecraft.world.entity.EntityDimensions
import net.minecraft.world.entity.Pose

open class UFOModeData : GDModeData() {

    override val mode: GDMode = GDMode.UFO

    private var targetCubeRot: Float = 0F
    private var cubeRot: Float = 0F
    private var prevCubeRot: Float = 0F

    companion object {
        @JvmField
        val CODEC: MapCodec<UFOModeData> = MapCodec.unit(::UFOModeData)
    }

    override fun tick() {
        if (this.gdData?.player?.level()?.isClientSide == true) {
            this.prevCubeRot = this.cubeRot
            this.gdData?.run {
                if (this.player.onGround()) {
                    this@UFOModeData.targetCubeRot = Math.round(this@UFOModeData.targetCubeRot / 90F) * 90F
                } else {
                    val gravity = this.player.gdGravity
                    this@UFOModeData.targetCubeRot += if (gravity.y < 0) -20 else 20
                }
            }
            this.cubeRot += (this.targetCubeRot - this.cubeRot) * 0.395F // both 0.395F and 0.45F seem alright, up to you tree
        }
    }

    override fun tickInput(): Boolean {
        val data = this.gdData!!
        val player = data.player
        if (data.canProcessInput) {
            player.launch(1.0)
            return true
        }
        return false
    }

    override fun getPose(): Pose {
        return GDPoses.CUBE!!
    }

    override fun getEntityDimensions(): EntityDimensions {
        return EntityDimensions.scalable(0.85F, 0.85F).withEyeHeight(0.425F)
    }

    override fun getCameraYOffset(): Float {
        return 0F
    }

    override fun getModelPitch(tickDelta: Float): Float {
        return Mth.lerp(tickDelta, this.prevCubeRot, this.cubeRot)
    }

    override val codec: MapCodec<out GDModeData> = CODEC

    override fun save(compound: CompoundTag): CompoundTag {
        return compound
    }

    override fun load(compound: CompoundTag): CompoundTag {
        return compound
    }
}
