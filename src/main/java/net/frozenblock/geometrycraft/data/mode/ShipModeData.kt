package net.frozenblock.geometrycraft.data.mode

import com.mojang.serialization.Codec
import com.mojang.serialization.MapCodec
import com.mojang.serialization.codecs.RecordCodecBuilder
import net.frozenblock.geometrycraft.data.GDMode
import net.frozenblock.geometrycraft.entity.pose.GDPoses
import net.frozenblock.geometrycraft.util.gdGravity
import net.minecraft.nbt.CompoundTag
import net.minecraft.util.Mth
import net.minecraft.world.entity.EntityDimensions
import net.minecraft.world.entity.Pose
import net.minecraft.world.phys.Vec3

open class ShipModeData(
    private var targetCubeRot: Float = 0F,
    private var cubeRot: Float = 0F,
    private var prevCubeRot: Float = 0F,
) : GDModeData() {

    override val mode: GDMode = GDMode.SHIP

    companion object {
        @JvmField
        val CODEC: MapCodec<ShipModeData> = RecordCodecBuilder.mapCodec { instance ->
            instance.group(
                Codec.FLOAT.fieldOf("target_rot").forGetter(ShipModeData::targetCubeRot),
                Codec.FLOAT.fieldOf("rot").forGetter(ShipModeData::cubeRot),
                Codec.FLOAT.fieldOf("prev_rot").forGetter(ShipModeData::prevCubeRot)
            ).apply(instance, ::ShipModeData)
        }
    }

    override fun tick() {
        if (this.gdData?.player?.level()?.isClientSide == true) {
            this.prevCubeRot = this.cubeRot
            this.gdData?.run {
                if (this.player.onGround()) {
                    this@ShipModeData.targetCubeRot = Math.round(this@ShipModeData.targetCubeRot / 90F) * 90F
                } else {
                    val gravity = this.player.gdGravity
                    this@ShipModeData.targetCubeRot += if (gravity.y < 0) -24 else 24
                }
            }
            this.cubeRot += (this.targetCubeRot - this.cubeRot) * 0.395F // both 0.395F and 0.45F seem alright, up to you tree
        }
    }

    override fun tickInput(): Boolean {
        val data = this.gdData!!
        val player = data.player
        val delta = player.deltaMovement
        if (data.canProcessInput) {
            player.addDeltaMovement(Vec3(0.0, 0.4, 0.0))
            player.hasImpulse = true
            return true
        }
        return false
    }

    override fun lockOnSuccess(): Boolean {
        return false
    }

    override fun getPose(): Pose {
        return GDPoses.CUBE!!
    }

    override fun getEntityDimensions(): EntityDimensions {
        return EntityDimensions.scalable(0.85F, 0.85F).withEyeHeight(0.425F)
    }

    override fun getCameraYOffset(): Float {
        return 0F
    }

    override fun getModelPitch(tickDelta: Float): Float {
        return Mth.lerp(tickDelta, this.prevCubeRot, this.cubeRot)
    }

    override val codec: MapCodec<out GDModeData> = CODEC

    override fun save(compound: CompoundTag): CompoundTag {
        return compound
    }

    override fun load(compound: CompoundTag): CompoundTag {
        return compound
    }
}
