package net.frozenblock.geometrycraft.data.mode

import com.mojang.serialization.Codec
import com.mojang.serialization.MapCodec
import net.frozenblock.geometrycraft.data.GDMode

open class Robot3DModeData : RobotModeData() {

    override val mode: GDMode = GDMode.ROBOT_3D

    companion object {
        @JvmField
        val CODEC: MapCodec<Robot3DModeData> = MapCodec.unit(::Robot3DModeData)
    }

    override val withstandsCollisions: Boolean = true

    override val codec: MapCodec<out GDModeData> = CODEC
}
