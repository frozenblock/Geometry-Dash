package net.frozenblock.geometrycraft.datafix

import net.fabricmc.loader.api.ModContainer
import net.frozenblock.geometrycraft.datafix.schemas.*
import net.frozenblock.geometrycraft.util.*
import net.minecraft.core.Direction
import net.minecraft.util.datafix.DataFixers
import net.minecraft.util.datafix.fixes.AddNewChoices
import net.minecraft.util.datafix.fixes.BlockEntityRenameFix
import net.minecraft.util.datafix.fixes.References
import net.minecraft.util.datafix.schemas.NamespacedSchema
import net.minecraft.world.level.block.state.properties.BlockStateProperties
import org.quiltmc.qsl.frozenblock.misc.datafixerupper.api.QuiltDataFixerBuilder
import org.quiltmc.qsl.frozenblock.misc.datafixerupper.api.QuiltDataFixes
import org.quiltmc.qsl.frozenblock.misc.datafixerupper.api.SimpleFixes

const val DATA_VERSION = 6

object GDDataFixer {

    fun applyDataFixes(mod: ModContainer) {
        val builder = QuiltDataFixerBuilder(DATA_VERSION)
        builder.addSchema(0, QuiltDataFixes.BASE_SCHEMA)

        val schemaV1 = builder.addSchema(1, ::GCV1)
        builder.addFixer(AddNewChoices(schemaV1, oldString("jump_pad"), References.BLOCK_ENTITY))
        builder.addFixer(AddNewChoices(schemaV1, oldString("checkpoint"), References.ENTITY))
        builder.addFixer(AddNewChoices(schemaV1, oldString("orb"), References.ENTITY))
        builder.addFixer(AddNewChoices(schemaV1, oldString("portal"), References.ENTITY))
        SimpleFixes.addBlockStateRenameFix(builder, "Rename tip direction to facing", oldId("spike"), BlockStateProperties.VERTICAL_DIRECTION.name, Direction.UP.serializedName, BlockStateProperties.FACING.name, schemaV1)
        val schemaV2 = builder.addSchema(2, ::NamespacedSchema)
        SimpleFixes.addEntityRenameFix(builder, "Rename Ring to Orb", oldId("ring"), oldId("orb"), schemaV2)

        val schemaV3 = builder.addSchema(3, ::GCV3)
        SimpleFixes.addBlockRenameFix(builder, "Swap high jump pad namespace", oldId("high_jump_pad"), midId("high_jump_pad"), schemaV3)
        SimpleFixes.addItemRenameFix(builder, "Swap high jump pad namespace", oldId("high_jump_pad"), midId("high_jump_pad"), schemaV3)
        SimpleFixes.addBlockRenameFix(builder, "Swap jump pad namespace", oldId("jump_pad"), midId("jump_pad"), schemaV3)
        SimpleFixes.addItemRenameFix(builder, "Swap jump pad namespace", oldId("jump_pad"), midId("jump_pad"), schemaV3)
        SimpleFixes.addBlockRenameFix(builder, "Swap low jump pad namespace", oldId("low_jump_pad"), midId("low_jump_pad"), schemaV3)
        SimpleFixes.addItemRenameFix(builder, "Swap low jump pad namespace", oldId("low_jump_pad"), midId("low_jump_pad"), schemaV3)
        SimpleFixes.addBlockRenameFix(builder, "Swap reverse gravity jump pad namespace", oldId("reverse_gravity_jump_pad"), midId("reverse_gravity_jump_pad"), schemaV3)
        SimpleFixes.addItemRenameFix(builder, "Swap reverse gravity jump pad namespace", oldId("reverse_gravity_jump_pad"), midId("reverse_gravity_jump_pad"), schemaV3)
        SimpleFixes.addBlockRenameFix(builder, "Swap spike namespace", oldId("spike"), midId("spike"), schemaV3)
        SimpleFixes.addItemRenameFix(builder, "Swap spike namespace", oldId("spike"), midId("spike"), schemaV3)
        SimpleFixes.addBlockRenameFix(builder, "Swap teleport pad namespace", oldId("teleport_pad"), midId("teleport_pad"), schemaV3)
        SimpleFixes.addItemRenameFix(builder, "Swap teleport pad namespace", oldId("teleport_pad"), midId("teleport_pad"), schemaV3)
        builder.addFixer(BlockEntityRenameFix.create(schemaV3, "Swap jump pad block entity namespace", DataFixers.createRenamer(mapOf(oldString("jump_pad") to midString("jump_pad")))))
        val schemaV4 = builder.addSchema(4, ::NamespacedSchema)
        SimpleFixes.addBiomeRenameFix(builder, "Swap geometry biome namespace", mapOf(oldId("geometry") to midId("geometry")), schemaV4)
        val schemaV5 = builder.addSchema(5, ::GCV5)
        SimpleFixes.addEntityRenameFix(builder, "Swap checkpoint namespace", oldId("checkpoint"), midId("checkpoint"), schemaV5)
        SimpleFixes.addEntityRenameFix(builder, "Swap orb namespace", oldId("orb"), midId("orb"), schemaV5)
        SimpleFixes.addEntityRenameFix(builder, "Swap portal namespace", oldId("portal"), midId("portal"), schemaV5)
        val schemaV6 = builder.addSchema(6, ::GCV6)
        SimpleFixes.addBlockRenameFix(builder, "Swap high jump pad namespace again", midId("high_jump_pad"), id("high_jump_pad"), schemaV6)
        SimpleFixes.addItemRenameFix(builder, "Swap high jump pad namespace again", midId("high_jump_pad"), id("high_jump_pad"), schemaV6)
        SimpleFixes.addBlockRenameFix(builder, "Swap jump pad namespace again", midId("jump_pad"), id("jump_pad"), schemaV6)
        SimpleFixes.addItemRenameFix(builder, "Swap jump pad namespace again", midId("jump_pad"), id("jump_pad"), schemaV6)
        SimpleFixes.addBlockRenameFix(builder, "Swap low jump pad namespace again", midId("low_jump_pad"), id("low_jump_pad"), schemaV6)
        SimpleFixes.addItemRenameFix(builder, "Swap low jump pad namespace again", midId("low_jump_pad"), id("low_jump_pad"), schemaV6)
        SimpleFixes.addBlockRenameFix(builder, "Swap reverse gravity jump pad namespace again", midId("reverse_gravity_jump_pad"), id("reverse_gravity_jump_pad"), schemaV6)
        SimpleFixes.addItemRenameFix(builder, "Swap reverse gravity jump pad namespace again", midId("reverse_gravity_jump_pad"), id("reverse_gravity_jump_pad"), schemaV6)
        SimpleFixes.addBlockRenameFix(builder, "Swap spike namespace again", midId("spike"), id("spike"), schemaV6)
        SimpleFixes.addItemRenameFix(builder, "Swap spike namespace again", midId("spike"), id("spike"), schemaV6)
        SimpleFixes.addBlockRenameFix(builder, "Swap teleport pad namespace again", midId("teleport_pad"), id("teleport_pad"), schemaV6)
        SimpleFixes.addItemRenameFix(builder, "Swap teleport pad namespace again", midId("teleport_pad"), id("teleport_pad"), schemaV6)
        builder.addFixer(BlockEntityRenameFix.create(schemaV6, "Swap jump pad block entity namespace again", DataFixers.createRenamer(mapOf(midString("jump_pad") to string("jump_pad")))))
        SimpleFixes.addBiomeRenameFix(builder, "Swap geometry biome namespace again", mapOf(midId("geometry") to id("geometry")), schemaV6)
        SimpleFixes.addEntityRenameFix(builder, "Swap checkpoint namespace again", midId("checkpoint"), id("checkpoint"), schemaV6)
        SimpleFixes.addEntityRenameFix(builder, "Swap orb namespace again", midId("orb"), midId("orb"), schemaV6)
        SimpleFixes.addEntityRenameFix(builder, "Swap portal namespace again", midId("portal"), midId("portal"), schemaV6)

        QuiltDataFixes.buildAndRegisterFixer(mod, builder)
    }
}
