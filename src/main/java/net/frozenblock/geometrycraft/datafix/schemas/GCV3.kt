package net.frozenblock.geometrycraft.datafix.schemas

import com.mojang.datafixers.schemas.Schema
import com.mojang.datafixers.types.templates.TypeTemplate
import net.frozenblock.geometrycraft.util.midString
import net.frozenblock.geometrycraft.util.oldString
import net.minecraft.util.datafix.schemas.NamespacedSchema
import java.util.function.Supplier

open class GCV3(versionKey: Int, parent: Schema) : NamespacedSchema(versionKey, parent) {

    override fun registerBlockEntities(schema: Schema): MutableMap<String, Supplier<TypeTemplate>> {
        val map = super.registerBlockEntities(schema)
        map[midString("jump_pad")] = map.remove(oldString("jump_pad"))
        return map
    }
}
