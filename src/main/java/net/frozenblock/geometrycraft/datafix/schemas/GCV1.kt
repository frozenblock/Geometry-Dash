package net.frozenblock.geometrycraft.datafix.schemas

import com.mojang.datafixers.schemas.Schema
import com.mojang.datafixers.types.templates.TypeTemplate
import net.frozenblock.geometrycraft.util.oldString
import net.minecraft.util.datafix.schemas.NamespacedSchema
import java.util.function.Supplier

open class GCV1(versionKey: Int, parent: Schema) : NamespacedSchema(versionKey, parent) {

    override fun registerBlockEntities(schema: Schema): MutableMap<String, Supplier<TypeTemplate>> {
        val map = super.registerBlockEntities(schema)
        schema.registerSimple(map, oldString("jump_pad"))
        return map
    }

    override fun registerEntities(schema: Schema): MutableMap<String, Supplier<TypeTemplate>> {
        val map = super.registerEntities(schema)
        schema.registerSimple(map, oldString("checkpoint"))
        schema.registerSimple(map, oldString("orb"))
        schema.registerSimple(map, oldString("portal"))
        return map
    }
}
