package net.frozenblock.geometrycraft.entity.data

import net.minecraft.core.Direction
import net.minecraft.network.codec.ByteBufCodecs
import net.minecraft.network.syncher.EntityDataAccessor
import net.minecraft.network.syncher.EntityDataSerializer
import net.minecraft.network.syncher.EntityDataSerializers
import net.minecraft.network.syncher.SynchedEntityData
import net.minecraft.world.entity.Entity

object GCEntityData {

    @JvmField
    val DOUBLE_SERIALIZER: EntityDataSerializer<Double> = EntityDataSerializer.forValueType(ByteBufCodecs.DOUBLE)

    @JvmField
    val GRAVITY_STRENGTH: EntityDataAccessor<Double> = SynchedEntityData.defineId(Entity::class.java, DOUBLE_SERIALIZER)

    @JvmField
    val GRAVITY_DIRECTION: EntityDataAccessor<Direction> = SynchedEntityData.defineId(Entity::class.java, EntityDataSerializers.DIRECTION)

    init {
        EntityDataSerializers.registerSerializer(DOUBLE_SERIALIZER)
    }
}
