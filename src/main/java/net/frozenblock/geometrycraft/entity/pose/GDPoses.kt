package net.frozenblock.geometrycraft.entity.pose

import net.minecraft.world.entity.Pose

object GDPoses {
    @JvmField
	var CUBE: Pose? = null

    //CREDIT TO nyuppo/fabric-boat-example ON GITHUB
    init {
        Pose.entries
    }
}
