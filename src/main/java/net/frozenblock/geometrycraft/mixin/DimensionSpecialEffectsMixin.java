package net.frozenblock.geometrycraft.mixin;

import it.unimi.dsi.fastutil.objects.Object2ObjectArrayMap;
import net.frozenblock.geometrycraft.GeometryCraft;
import net.frozenblock.geometrycraft.worldgen.GeometrySpecialEffects;
import net.minecraft.client.renderer.DimensionSpecialEffects;
import net.minecraft.resources.ResourceLocation;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(DimensionSpecialEffects.class)
public class DimensionSpecialEffectsMixin {

	@Inject(method = "method_29092", at = @At("TAIL"))
	private static void addGDEffects(Object2ObjectArrayMap<ResourceLocation, DimensionSpecialEffects> map, CallbackInfo ci) {
		map.put(GeometryCraft.SPECIAL_EFFECTS, new GeometrySpecialEffects());
	}
}
