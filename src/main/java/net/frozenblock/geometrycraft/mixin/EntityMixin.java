package net.frozenblock.geometrycraft.mixin;

import com.llamalad7.mixinextras.injector.v2.WrapWithCondition;
import com.llamalad7.mixinextras.injector.wrapoperation.Operation;
import com.llamalad7.mixinextras.injector.wrapoperation.WrapOperation;
import net.frozenblock.geometrycraft.data.GDData;
import net.frozenblock.geometrycraft.duck.EntityDuck;
import net.frozenblock.geometrycraft.duck.PlayerDuck;
import net.frozenblock.geometrycraft.network.C2SFailPacket;
import net.frozenblock.geometrycraft.entity.data.GCEntityData;
import net.frozenblock.geometrycraft.util.GDSharedConstantsKt;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.DustParticleOptions;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MoverType;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.phys.Vec3;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(Entity.class)
public abstract class EntityMixin implements EntityDuck {

	@Unique
	private static final EntityDataAccessor<Double> GRAVITY_STRENGTH = GCEntityData.GRAVITY_STRENGTH;

	@Unique
	private static final EntityDataAccessor<Direction> GRAVITY_DIRECTION = GCEntityData.GRAVITY_DIRECTION;

	@Shadow
	public boolean horizontalCollision;

	@Shadow
	@Final
	protected SynchedEntityData entityData;

	@Shadow
	public boolean minorHorizontalCollision;

	@Shadow
	public abstract boolean onGround();

	@Shadow
	public abstract Level level();

	@WrapWithCondition(
		method = "checkFallDamage",
		at = @At(value = "INVOKE", target = "Lnet/minecraft/world/level/block/Block;fallOn(Lnet/minecraft/world/level/Level;Lnet/minecraft/world/level/block/state/BlockState;Lnet/minecraft/core/BlockPos;Lnet/minecraft/world/entity/Entity;F)V")
	)
	private boolean preventGDFallDamage(Block instance, Level level, BlockState state, BlockPos pos, Entity entity, float fallDistance) {
        return !(this instanceof PlayerDuck duck) || !duck.geometryDash$getGDData().getPlayingGD();
    }

	@WrapOperation(method = "spawnSprintParticle", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/level/Level;addParticle(Lnet/minecraft/core/particles/ParticleOptions;DDDDDD)V"))
	private void gdSprintParticles(Level instance, ParticleOptions particleData, double x, double y, double z, double xSpeed, double ySpeed, double zSpeed, Operation<Void> original) {
		ParticleOptions particleOptions = (this instanceof PlayerDuck duck && duck.geometryDash$getGDData().getPlayingGD()) ? DustParticleOptions.REDSTONE : particleData;
		original.call(instance, particleOptions, x, y, z, xSpeed, ySpeed, zSpeed);
	}

	@Inject(method = "tick", at = @At("TAIL"))
	private void gdCheck(CallbackInfo ci) {
		Entity entity = Entity.class.cast(this);
		if (entity.level().isClientSide && this instanceof PlayerDuck duck && entity instanceof Player player) {
			GDData data = duck.geometryDash$getGDData();
			if (!player.isDeadOrDying() && data.getPlayingGD() && !data.getModeData().getWithstandsCollisions() && this.horizontalCollision && !this.minorHorizontalCollision) {
				player.setHealth(0);
				C2SFailPacket.Companion.send$GeometryCraft();
			}
		}
	}

	@Inject(method = "move", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/entity/Entity;setOnGroundWithMovement(ZLnet/minecraft/world/phys/Vec3;)V", shift = At.Shift.AFTER))
	private void instantGDInput(MoverType type, Vec3 pos, CallbackInfo ci) {
		if (this.level().isClientSide && this instanceof PlayerDuck duck) {
			var data = duck.geometryDash$getGDData();
			if (this.onGround()) {
				var mode = data.getModeData();
				if (mode != null)
					mode.tickInput();
			}
		}
	}

	@WrapOperation(method = "<init>", at = @At(value = "INVOKE", target = "Lnet/minecraft/network/syncher/SynchedEntityData$Builder;build()Lnet/minecraft/network/syncher/SynchedEntityData;"))
	private SynchedEntityData addGravityData(SynchedEntityData.Builder builder, Operation<SynchedEntityData> original) {
		builder.define(GRAVITY_STRENGTH, GDSharedConstantsKt.DEFAULT_GRAVITY_STRENGTH);
		builder.define(GRAVITY_DIRECTION, GDSharedConstantsKt.DEFAULT_GRAVITY_DIRECTION);
		return original.call(builder);
	}

	@SuppressWarnings("ConstantValue")
	@Inject(method = "saveWithoutId", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/entity/Entity;addAdditionalSaveData(Lnet/minecraft/nbt/CompoundTag;)V"))
	private void saveGravity(CompoundTag compound, CallbackInfoReturnable<CompoundTag> cir) {
		double gravityStrength = this.entityData.get(GRAVITY_STRENGTH);
		Direction gravityDirection = this.entityData.get(GRAVITY_DIRECTION);
		if (gravityDirection == null) gravityDirection = GDSharedConstantsKt.DEFAULT_GRAVITY_DIRECTION;
		compound.putDouble("gravity_strength", gravityStrength);
		compound.putString("gravity_direction", gravityDirection.getSerializedName());
	}

	@Inject(method = "load", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/entity/Entity;readAdditionalSaveData(Lnet/minecraft/nbt/CompoundTag;)V"))
	private void loadGravity(CompoundTag compound, CallbackInfo ci) {
		double strength = compound.contains("gravity_strength") ? compound.getDouble("gravity_strength") : GDSharedConstantsKt.DEFAULT_GRAVITY_STRENGTH;
		this.entityData.set(GRAVITY_STRENGTH, strength);

		Direction direction = compound.contains("gravity_direction") ? Direction.byName(compound.getString("gravity_direction")) : GDSharedConstantsKt.DEFAULT_GRAVITY_DIRECTION;
		this.entityData.set(GRAVITY_DIRECTION, direction);
	}

	@Unique
	@Override
	public double geometryDash$getGravityStrength() {
		return this.entityData.get(GRAVITY_STRENGTH);
	}

	@Unique
	@Override
	public void geometryDash$setGravityStrength(double strength) {
		this.entityData.set(GRAVITY_STRENGTH, strength, true);
	}

	@Unique
	@Override
	@NotNull
	public Direction geometryDash$getGravityDirection() {
		return this.entityData.get(GRAVITY_DIRECTION);
	}

	@Unique
	@Override
	public void geometryDash$setGravityDirection(@NotNull Direction direction) {
		this.entityData.set(GRAVITY_DIRECTION, direction, true);
	}
}
