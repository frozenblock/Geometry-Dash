package net.frozenblock.geometrycraft.mixin.audio;

import de.keksuccino.melody.resources.audio.openal.ALAudioClip;
import net.frozenblock.geometrycraft.duck.GDClip;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(ALAudioClip.class)
public class ALAudioClipMixin implements GDClip {

	@Final
	@Shadow(remap = false)
	protected int source;

	@Override
	public int getGeometryDash$source() {
		return this.source;
	}
}
