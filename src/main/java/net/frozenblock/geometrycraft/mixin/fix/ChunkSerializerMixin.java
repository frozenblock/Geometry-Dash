package net.frozenblock.geometrycraft.mixin.fix;

import com.llamalad7.mixinextras.injector.wrapoperation.Operation;
import com.llamalad7.mixinextras.injector.wrapoperation.WrapOperation;
import net.frozenblock.geometrycraft.GeometryCraft;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.StringTag;
import net.minecraft.world.level.chunk.storage.ChunkSerializer;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;

@Mixin(ChunkSerializer.class)
public class ChunkSerializerMixin {

	@WrapOperation(method = "read", at = @At(value = "INVOKE", target = "Lnet/minecraft/nbt/CompoundTag;getCompound(Ljava/lang/String;)Lnet/minecraft/nbt/CompoundTag;", ordinal = 2))
	private static CompoundTag modifyBiomes(CompoundTag instance, String key, Operation<CompoundTag> original) {
		var biomes = original.call(instance, key);
		var palette = biomes.getList("palette", CompoundTag.TAG_STRING);
		StringTag oldDimension = StringTag.valueOf(GeometryCraft.OLD_DIMENSION.toString());
		StringTag midDimension = StringTag.valueOf(GeometryCraft.MID_DIMENSION.toString());
		if (palette.contains(oldDimension) || palette.contains(midDimension)) {
			palette.setTag(palette.indexOf(oldDimension), StringTag.valueOf(GeometryCraft.DIMENSION.location().toString()));
		}
		return biomes;
	}
}
