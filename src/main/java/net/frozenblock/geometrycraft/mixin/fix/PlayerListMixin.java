package net.frozenblock.geometrycraft.mixin.fix;

import net.frozenblock.geometrycraft.GeometryCraft;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.players.PlayerList;
import net.minecraft.world.level.Level;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import java.util.Objects;
import java.util.Optional;

@Mixin(PlayerList.class)
public class PlayerListMixin {

	@Inject(method = "method_55634", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/level/dimension/DimensionType;parseLegacy(Lcom/mojang/serialization/Dynamic;)Lcom/mojang/serialization/DataResult;", shift = At.Shift.BEFORE))
	private static void fixDimension(CompoundTag compound, CallbackInfoReturnable<Optional<ResourceKey<Level>>> cir) {
		String dimension = compound.getString("Dimension");
		if (Objects.equals(dimension, GeometryCraft.OLD_DIMENSION.toString()) || Objects.equals(dimension, GeometryCraft.MID_DIMENSION.toString())) {
			compound.putString("Dimension", GeometryCraft.DIMENSION.location().toString());
		}
	}
}
