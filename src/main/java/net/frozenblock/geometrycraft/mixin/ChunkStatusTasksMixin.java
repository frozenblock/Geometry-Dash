package net.frozenblock.geometrycraft.mixin;

import net.frozenblock.geometrycraft.worldgen.SchematicChunkGenerator;
import net.minecraft.server.level.GenerationChunkHolder;
import net.minecraft.util.StaticCache2D;
import net.minecraft.world.level.chunk.ChunkAccess;
import net.minecraft.world.level.chunk.status.ChunkStatusTasks;
import net.minecraft.world.level.chunk.status.ChunkStep;
import net.minecraft.world.level.chunk.status.WorldGenContext;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executor;

@Mixin(ChunkStatusTasks.class)
public class ChunkStatusTasksMixin {

	@Inject(method = "generateNoise", at = @At("HEAD"))
	private static void setSchematicLevel(WorldGenContext context, ChunkStep chunkStep, StaticCache2D<GenerationChunkHolder> staticCache2D, ChunkAccess loadingChunk, CallbackInfoReturnable<CompletableFuture<ChunkAccess>> cir) {
		if (context.generator() instanceof SchematicChunkGenerator schem) {
			schem.init(context.level());
		}
	}
}
