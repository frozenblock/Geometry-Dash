package net.frozenblock.geometrycraft.mixin;

import com.llamalad7.mixinextras.injector.wrapoperation.Operation;
import com.llamalad7.mixinextras.injector.wrapoperation.WrapOperation;
import com.llamalad7.mixinextras.sugar.Share;
import com.llamalad7.mixinextras.sugar.ref.LocalBooleanRef;
import java.util.Optional;
import net.frozenblock.geometrycraft.data.CheckpointSnapshot;
import net.frozenblock.geometrycraft.data.GDData;
import net.frozenblock.geometrycraft.duck.PlayerDuck;
import net.frozenblock.geometrycraft.entity.Checkpoint;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.server.players.PlayerList;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.phys.Vec3;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(PlayerList.class)
public class PlayerListMixin {

	@Inject(method = "respawn", at = @At("RETURN"))
	private void restoreCheckpoint(ServerPlayer player, boolean b, Entity.RemovalReason reason, CallbackInfoReturnable<ServerPlayer> cir) {
		GDData data = ((PlayerDuck) player).geometryDash$getGDData();
		CheckpointSnapshot lastCheckpoint = data.getLastValidCheckpoint();
		if (lastCheckpoint != null) {
			Entity entity = data.getLevel().getEntity(lastCheckpoint.entityId);
			if (entity instanceof Checkpoint checkpoint) {
				CheckpointSnapshot.Companion.restoreCheckpoint(player, data, checkpoint, lastCheckpoint);
			}
		}
	}
}
