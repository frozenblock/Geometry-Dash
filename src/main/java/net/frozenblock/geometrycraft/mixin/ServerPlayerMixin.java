package net.frozenblock.geometrycraft.mixin;

import com.llamalad7.mixinextras.injector.ModifyReturnValue;
import net.frozenblock.geometrycraft.GeometryCraft;
import net.frozenblock.geometrycraft.data.CheckpointSnapshot;
import net.frozenblock.geometrycraft.duck.PlayerDuck;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceKey;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.level.Level;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(ServerPlayer.class)
public class ServerPlayerMixin {

	@ModifyReturnValue(method = "getRespawnDimension", at = @At("RETURN"))
	private ResourceKey<Level> spawnInGD(ResourceKey<Level> original) {
		var data = ((PlayerDuck) this).geometryDash$getGDData();
		if (data.getPlayingGD())
			return GeometryCraft.DIMENSION;
		return original;
	}

	@ModifyReturnValue(method = "getRespawnPosition", at = @At("RETURN"))
	private BlockPos spawnAtCheckpoint(BlockPos original) {
		var data = ((PlayerDuck) this).geometryDash$getGDData();
		CheckpointSnapshot lastCheckpoint = data.getLastValidCheckpoint();
		if (lastCheckpoint != null) {
			Entity entity = data.getLevel().getEntity(lastCheckpoint.entityId);
			if (entity != null) {
				return entity.blockPosition();
			}
		}
		return original;
	}

	@ModifyReturnValue(method = "getRespawnAngle", at = @At("RETURN"))
	private float checkpointAngle(float original) {
		var data = ((PlayerDuck) this).geometryDash$getGDData();
		CheckpointSnapshot lastCheckpoint = data.getLastValidCheckpoint();
		if (lastCheckpoint != null) {
			Entity entity = data.getLevel().getEntity(lastCheckpoint.entityId);
			if (entity != null) {
				return lastCheckpoint.yRot;
			}
		}
		return original;
	}

	@Inject(method = "restoreFrom", at = @At("TAIL"))
	private void restoreGD(ServerPlayer that, boolean keepEverything, CallbackInfo ci) {
		var thisDuck = (PlayerDuck) this;
		var thisData = thisDuck.geometryDash$getGDData();
		var thatDuck = (PlayerDuck) that;
		var thatData = thatDuck.geometryDash$getGDData();

		thisData.copyFrom(thatData);
		thisData.markDirty();
	}
}
