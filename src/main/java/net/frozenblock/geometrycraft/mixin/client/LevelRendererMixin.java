package net.frozenblock.geometrycraft.mixin.client;

import com.mojang.blaze3d.vertex.PoseStack;
import net.frozenblock.geometrycraft.data.GDData;
import net.frozenblock.geometrycraft.duck.PlayerDuck;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.Camera;
import net.minecraft.client.DeltaTracker;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.GameRenderer;
import net.minecraft.client.renderer.LevelRenderer;
import net.minecraft.client.renderer.LightTexture;
import org.joml.Matrix4f;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Environment(EnvType.CLIENT)
@Mixin(LevelRenderer.class)
public class LevelRendererMixin {

	@Inject(method = "renderLevel", at = @At("HEAD"))
	private void flipCam(DeltaTracker tracker, boolean renderBlockOutline, Camera camera, GameRenderer gameRenderer, LightTexture lightTexture, Matrix4f projectionMatrix, Matrix4f frustrumMatrix, CallbackInfo ci) {
		// TODO: Mirror everything properly, then use it after going through a mirror portal
		//poseStack.scale(-1, 1, 1);
		if (Minecraft.getInstance().player instanceof PlayerDuck duck) {
			GDData data = duck.geometryDash$getGDData();
			if (data.getPlayingGD())
				projectionMatrix.scale(data.cameraMirrorProgress, 1, 1);
		}
	}
}
