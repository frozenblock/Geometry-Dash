package net.frozenblock.geometrycraft.mixin.client;

import de.keksuccino.melody.resources.audio.openal.ALAudioClip;
import net.frozenblock.geometrycraft.duck.MCDuck;
import net.frozenblock.geometrycraft.duck.PlayerDuck;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screens.Screen;
import net.minecraft.client.multiplayer.ClientLevel;
import net.minecraft.client.player.LocalPlayer;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(Minecraft.class)
public class MinecraftMixin implements MCDuck {

	@Shadow
	@Nullable
	public Screen screen;
	@Shadow
	@Nullable
	public ClientLevel level;
	@Shadow
	@Nullable
	public LocalPlayer player;
	@Shadow
	static Minecraft instance;
	@Unique
	@Nullable
	private ALAudioClip audioClip = null;

	@Inject(method = "runTick", at = @At("TAIL"))
	private void controlPause(boolean renderLevel, CallbackInfo ci) {
		var audioClip = this.audioClip;
		if (audioClip != null) {
			try {
				if (this.screen == null) {
					audioClip.resume();
				} else {
					if (this.level == null) {
						audioClip.stop();
						audioClip.closeQuietly();
						this.audioClip = null;
					} else audioClip.pause();
				}
			} catch (Exception ignored) {}
		}
	}

	@Inject(method = "openChatScreen", at = @At("HEAD"), cancellable = true)
	private void cancelGDChat(String defaultText, CallbackInfo ci) {
		if (((PlayerDuck) this.player).geometryDash$getGDData().getPlayingGD())
			ci.cancel();
	}

	@Nullable
	@Override
	public ALAudioClip getGeometryDash$audioClip() {
		return this.audioClip;
	}

	@Override
	public void setGeometryDash$audioClip(@Nullable ALAudioClip value) {
		var cur = this.audioClip;
		if (cur != null && cur != value && cur.isValidOpenAlSource()) {
			try {
				cur.stop();
			} catch (Exception ignored) {}
			cur.closeQuietly();
		}
		this.audioClip = value;
	}
}
