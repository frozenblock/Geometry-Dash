package net.frozenblock.geometrycraft.mixin.gravity;

import com.llamalad7.mixinextras.injector.wrapoperation.Operation;
import com.llamalad7.mixinextras.injector.wrapoperation.WrapOperation;
import net.frozenblock.geometrycraft.util.GDUtilsKt;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.phys.Vec3;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;

@Mixin(LivingEntity.class)
public class LivingEntityMixin {

	@WrapOperation(method = "jumpFromGround", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/entity/LivingEntity;setDeltaMovement(DDD)V"))
	private void gravityJump(LivingEntity instance, double x, double y, double z, Operation<Void> original) {
		Vec3 modified = GDUtilsKt.toRelative(instance, x, y, z);
 		original.call(instance, modified.x, modified.y, modified.z);
	}
}
