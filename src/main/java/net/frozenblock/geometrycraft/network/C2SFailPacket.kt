package net.frozenblock.geometrycraft.network

import net.fabricmc.fabric.api.client.networking.v1.ClientPlayNetworking
import net.frozenblock.geometrycraft.util.id
import net.frozenblock.geometrycraft.util.string
import net.minecraft.network.FriendlyByteBuf
import net.minecraft.network.codec.StreamCodec
import net.minecraft.network.protocol.common.custom.CustomPacketPayload

class C2SFailPacket() : CustomPacketPayload {
    companion object {
        @JvmField
        val TYPE: CustomPacketPayload.Type<C2SFailPacket> = CustomPacketPayload.Type(id("kill"))

        @JvmField
        val CODEC: StreamCodec<FriendlyByteBuf, C2SFailPacket> = StreamCodec.ofMember(C2SFailPacket::write, ::C2SFailPacket)

        internal fun send() {
            ClientPlayNetworking.send(C2SFailPacket())
        }
    }

    constructor(buf: FriendlyByteBuf) : this()

    fun write(buf: FriendlyByteBuf) {}

    override fun type(): CustomPacketPayload.Type<out CustomPacketPayload> = TYPE
}
