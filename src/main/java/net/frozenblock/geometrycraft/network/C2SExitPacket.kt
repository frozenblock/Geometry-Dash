package net.frozenblock.geometrycraft.network

import net.frozenblock.geometrycraft.util.id
import net.frozenblock.geometrycraft.util.string
import net.minecraft.network.FriendlyByteBuf
import net.minecraft.network.codec.StreamCodec
import net.minecraft.network.protocol.common.custom.CustomPacketPayload

class C2SExitPacket() : CustomPacketPayload {
    companion object {
        @JvmField
        val TYPE: CustomPacketPayload.Type<C2SExitPacket> = CustomPacketPayload.Type(id("exit"))

        @JvmField
        val CODEC: StreamCodec<FriendlyByteBuf, C2SExitPacket> = StreamCodec.ofMember(C2SExitPacket::write, ::C2SExitPacket)
    }

    constructor(buf: FriendlyByteBuf) : this()

    fun write(buf: FriendlyByteBuf?) {}

    override fun type(): CustomPacketPayload.Type<out CustomPacketPayload> = TYPE
}
