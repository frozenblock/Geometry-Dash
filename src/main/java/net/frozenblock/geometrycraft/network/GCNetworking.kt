package net.frozenblock.geometrycraft.network

import net.fabricmc.fabric.api.networking.v1.PayloadTypeRegistry

internal object GCNetworking {

    fun registerC2SPayloadTypes() {
        val registry = PayloadTypeRegistry.playC2S()

        registry.register(C2SExitPacket.TYPE, C2SExitPacket.CODEC)
        registry.register(C2SFailPacket.TYPE, C2SFailPacket.CODEC)
    }
}
