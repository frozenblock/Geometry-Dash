package net.frozenblock.geometrycraft.datagen

import net.frozenblock.geometrycraft.GeometryCraft
import net.frozenblock.geometrycraft.biome.TheOtherSideBiome
import net.fabricmc.fabric.api.datagen.v1.DataGeneratorEntrypoint
import net.fabricmc.fabric.api.datagen.v1.FabricDataGenerator
import net.minecraft.core.RegistrySetBuilder
import net.minecraft.core.registries.Registries
import net.minecraft.tags.BlockTags
import net.minecraft.util.valueproviders.ConstantInt
import net.minecraft.world.level.dimension.DimensionType
import java.util.*


object GDDatagen : DataGeneratorEntrypoint {
    override fun onInitializeDataGenerator(generator: FabricDataGenerator) {
        val pack = generator.createPack()
        Registries.BIOME_SOURCE

        pack.addProvider(::GDRegistryProvider)
        pack.addProvider(::GDBiomeTagProvider)

        pack.addProvider(::GDBlockLootProvider)
    }

    override fun buildRegistry(registryBuilder: RegistrySetBuilder) {
        registryBuilder.add(Registries.BIOME) { ctx ->
            ctx.register(
                TheOtherSideBiome.key,
                TheOtherSideBiome.create(ctx)
            )
        }

        registryBuilder.add(Registries.DIMENSION_TYPE) { ctx ->
            ctx.register(
                GeometryCraft.DIMENSION_TYPE, DimensionType(
                    OptionalLong.empty(),
                    true,
                    false,
                    false,
                    false,
                    1.0,
                    false,
                    false,
                    0,
                    1280,
                    1280,
                    BlockTags.INFINIBURN_OVERWORLD,
                    GeometryCraft.SPECIAL_EFFECTS,
                    0.0F,
                    DimensionType.MonsterSettings(true, false, ConstantInt.of(0), 0)
                )
            )
        }
    }
}
